package com.martinutils.profiletasker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProfileManager;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.widget.RemoteViews;

public class ProfileWidget extends AppWidgetProvider
{
    @Override
    public void onUpdate(Context context,
            AppWidgetManager appWidgetManager,
            int[] appWidgetIds)
    {
        onUpdate(context, appWidgetManager, appWidgetIds, true);
    }

    public void onUpdate(Context context,
            AppWidgetManager appWidgetManager,
            int[] appWidgetIds,
            boolean first)
    {

        if (first)
        {
            AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent("com.martinutils.profiletasker.TRIGGERUPDATE");
            manager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + 5000,
                    5000,
                    PendingIntent.getBroadcast(context, 0, intent, 0));
        }

        final ProfileManager profileService = (ProfileManager) context.getSystemService(Context.PROFILE_SERVICE);
        for (int i = 0; i < appWidgetIds.length; i++)
        {
            int widgetID = appWidgetIds[i];
            RemoteViews rv = new RemoteViews(context.getPackageName(),
                    R.layout.profile_layout);
            rv.setTextViewText(R.id.WidgetLabel,
                    profileService.getActiveProfile().getName());

            Intent intent = new Intent(context, ProfileSwitch.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context,
                    0,
                    intent,
                    0);

            rv.setOnClickPendingIntent(R.id.WidgetContainer, pendingIntent);
            appWidgetManager.updateAppWidget(widgetID, rv);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction()
                .equals("com.martinutils.profiletasker.TRIGGERUPDATE"))
        {
            final AppWidgetManager instance = AppWidgetManager.getInstance(context);
            onUpdate(context,
                    instance,
                    instance.getAppWidgetIds(new ComponentName(context,
                            this.getClass().getName())),
                    false);
        }
        super.onReceive(context, intent);
    }
}
