package com.martinutils.profiletasker;

/**
 * Class of constants used by this <i>Locale</i> plug-in.
 */
final class Constants
{
    /**
     * Private constructor prevents instantiation
     * 
     * @throws UnsupportedOperationException
     *             because this class cannot be instantiated.
     */
    private Constants()
    {
        throw new UnsupportedOperationException(String.format("%s(): This class is non-instantiable", this.getClass().getSimpleName())); //$NON-NLS-1$
    }

    /**
     * Log tag for logcat messages
     */
    static final String LOG_TAG                              = "ProfileTasker";                              //$NON-NLS-1$

    /**
     * Type: {@code boolean}
     * <p>
     * SharedPreference key for retrieving a boolean as to whether the license
     * has been agreed to
     */
    static final String PREFERENCE_BOOLEAN_IS_LICENSE_AGREED = "IS_LICENSE_AGREED";                          //$NON-NLS-1$

    /**
     * Type: {@code String}
     * <p>
     * Maps to a {@code String} in the store-and-forward {@code Bundle}
     * {@link com.twofortyfouram.locale.Intent#EXTRA_BUNDLE}
     */
    static final String BUNDLE_EXTRA_STRING_PROFILE          = "com.martinutils.profiletasker.extra.PROFILE"; //$NON-NLS-1$

}
