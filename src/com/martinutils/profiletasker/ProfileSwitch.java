package com.martinutils.profiletasker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Profile;
import android.app.ProfileManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ProfileSwitch extends Activity
{

    ProfileManager profileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        profileManager = (ProfileManager) getSystemService(Context.PROFILE_SERVICE);

        setTheme(android.R.style.Theme_Translucent_NoTitleBar);

        final LinearLayout layout = new LinearLayout(getApplicationContext());
        final ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.icon));
        layout.addView(imageView);
        layout.setVisibility(View.GONE);
        setContentView(layout);

        showDialog(0);
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        final Profile[] profiles = profileManager.getProfiles();
        String activeProfile = profileManager.getActiveProfile().getName();
        final CharSequence[] names = new CharSequence[profiles.length];
        int i = 0;
        int checkedItem = 0;
        for (Profile profile : profiles)
        {
            if (profile.getName().equals(activeProfile))
            {
                checkedItem = i;
            }
            names[i++] = profile.getName();
        }

        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setSingleChoiceItems(names,
                checkedItem,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (which < 0)
                            return;
                        profileManager.setActiveProfile(profiles[which].getName());
                        dialog.cancel();
                        ProfileSwitch.this.finish();
                    }
                });
        ab.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog)
            {
                ProfileSwitch.this.finish();

            }
        });
        return ab.create();
    }

    @Override
    protected void onPause()
    {
        Intent intent = new Intent("com.martinutils.profiletasker.TRIGGERUPDATE");
        sendBroadcast(intent);
        super.onPause();
    }

}
