package com.martinutils.profiletasker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProfileManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ProfileActivity extends Activity
{

    ProfileManager profileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        profileManager = (ProfileManager) getSystemService(Context.PROFILE_SERVICE);

        setTheme(android.R.style.Theme_Translucent_NoTitleBar);

        final LinearLayout layout = new LinearLayout(getApplicationContext());
        final ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.icon));
        layout.addView(imageView);
        layout.setVisibility(View.GONE);
        setContentView(layout);

        showDialog(0);
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("Welcome to the Notification Profile Plugin");
        ab.setMessage("Thank you for purchasing the Notification Profile Plugin. The "
                + "widget is now available to be added to your homescreen. If you also "
                + "use Tasker, or Locale, the plugin is also available to be used by "
                + "these applications.");
        ab.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
                ProfileActivity.this.finish();
            }
        });
        return ab.create();
    }

}
